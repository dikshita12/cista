=== cista ===
Contributors: weblizar
Requires at least: 4.0
Tested up to: 5.5
Stable tag: 1.6
icense: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: two-columns, three-columns, four-columns, custom-menu, right-sidebar, custom-background, featured-image-header, sticky-post, theme-options, threaded-comments, featured-images, flexible-header, translation-ready 

Here is a short description of the theme.

Description: Child Theme of Enigma. This is a Boxed Layout of the Parent Theme Enigma. This theme supports Custom Background and Background Color.

==  Theme License & Copyright ==

cista is a child theme of Enigma WordPress Theme, Copyright 2020 weblizar and Enigma
cista is distributed under the terms of the GNU General Public License v2

License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Image Resources ==
= Screenshot =
Screen shot URL : https://pxhere.com/en/photo/285
All images are licensed under [CC0] (https://creativecommons.org/publicdomain/zero/1.0/)

Changelog 

Version 1.6
**update according to latest enigma

Version 1.5.4
**Minor Fixes.

Version 1.5.3
**Minor Fixes.


Version 1.5.2
*** Competible recent Enigma Update


Version 1.5.1
** Minor Fix in Menu


Version 1.5
1. Full Width Layout template added.

Version 1.4
1. Layout fixed for hire us page.

Version 1.3
1. Added new post template(left sidebar)

Version 1.2
1. Search box issue fixed.
2. Logo issue fixed.
3. Screenshot update.
4. Minor issues fixed.

Version 1.1
1.Minor changes in header. 
2.Search box issue fixed.
3.Screenshot changes.